# EASY

# Return the argument with all its lowercase characters removed.
def destructive_uppercase(str)
  char_arr = str.split('')

  char_arr.each_index do |i|
    if char_arr[i] == char_arr[i].downcase
      char_arr.delete_at(i)
    end
  end
  char_arr.join
end

# Return the middle character of a string. Return the middle two characters if
# the word is of even length, e.g. middle_substring("middle") => "dd",
# middle_substring("mid") => "i"
def middle_substring(str)
  len = str.length
  mid = len / 2

  if len.odd?
    str[mid]
  else
    str[mid - 1..mid]
  end
end

# Return the number of vowels in a string.
VOWELS = %w(a e i o u)
def num_vowels(str)
  str.chars.count { |ch| VOWELS.include?(ch)}
end

# Return the factoral of the argument (num). A number's factorial is the product
# of all whole numbers between 1 and the number itself. Assume the argument will
# be > 0.
def factorial(num)
  (1..num).reduce(:*)
end


# MEDIUM

# Write your own version of the join method. separator = "" ensures that the
# default seperator is an empty string.
def my_join(arr, separator = "")
  result = ''
  arr.each_with_index do |ch, i|
    result << ch
    if i < arr.length - 1
      result << separator
    end
  end
  result
end

# Write a method that converts its argument to weirdcase, where every odd
# character is lowercase and every even is uppercase, e.g.
# weirdcase("weirdcase") => "wEiRdCaSe"
def weirdcase(str)
  result = ''
  str.chars.each_with_index do |ch, i|
    if i.odd?
      result << ch.upcase
    else
      result << ch.downcase
    end
  end

  result
end

# Reverse all words of five more more letters in a string. Return the resulting
# string, e.g., reverse_five("Looks like my luck has reversed") => "skooL like
# my luck has desrever")
def reverse_five(str)
  result = []

  str.split(' ').each do |w|
    if five_or_more?(w)
      result << w.reverse
    else
      result << w
    end
  end

  result.join(' ')
end

def five_or_more?(str)
  str.length >= 5 ? true : false
end

# Return an array of integers from 1 to n (inclusive), except for each multiple
# of 3 replace the integer with "fizz", for each multiple of 5 replace the
# integer with "buzz", and for each multiple of both 3 and 5, replace the
# integer with "fizzbuzz".
def fizzbuzz(n)
  result = []

  (1..n).each do |num|
    if ((num % 3 == 0) && (num % 5 == 0))
      result << 'fizzbuzz'
    elsif num % 3 == 0
      result << 'fizz'
    elsif num % 5 == 0
      result << 'buzz'
    else
      result << num
    end
  end

  result
end

# HARD

# Write a method that returns a new array containing all the elements of the
# original array in reverse order.
def my_reverse(arr)
  arr.reverse
end

# Write a method that returns a boolean indicating whether the argument is
# prime.
def prime?(num)

  if num == 1
    return false
  end

  (2..(num - 1)).each do |n|
    if num % n == 0
      return false
    end
  end

  true
end

# Write a method that returns a sorted array of the factors of its argument.
def factors(num)
  factors = []

  (1..num).each do |n|
    if num % n == 0
      factors << n
    end
  end

  factors.sort
end

# Write a method that returns a sorted array of the prime factors of its argument.
def prime_factors(num)
  factors(num).select { |n| prime?(n)}.sort
end

# Write a method that returns the number of prime factors of its argument.
def num_prime_factors(num)
  prime_factors(num).count
end


# EXPERT

# Return the one integer in an array that is even or odd while the rest are of
# opposite parity, e.g. oddball([1,2,3]) => 2, oddball([2,4,5,6] => 5)
def oddball(arr)
  num_odds = arr.count { |n| n.odd? }

  if num_odds == 1
    arr.select { |num| num.odd? }[0]
  else
    arr.select { |num| num.odd? == false }[0]
  end

end
